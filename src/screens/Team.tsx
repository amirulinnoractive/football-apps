import React,{useState, useEffect} from 'react'
import { Button, View, Text, Image, StyleSheet,ScrollView, Alert } from 'react-native';
import axios from 'axios'
import {SvgUri, SvgXml, SvgCssUri} from 'react-native-svg'
import AsyncStorage from '@react-native-community/async-storage'

const styles = StyleSheet.create({
  box:{
      width: '100%',
      height: '50%',
      alignContent: 'center',
      backgroundColor: '#8E1F2F',
      borderBottomLeftRadius: 20,
      // borderEndColor: '#F0BC42',
      borderBottomRightRadius: 20,
      
  }
})

interface Team{
  data: {
    teamId:number | string,
    navigation: any,
  }
}

function Teams(props: any) {

  const api = axios.create({
    baseURL: 'https://api.football-data.org/v2/',
    headers: {'X-Auth-Token': 'a9c38bc1a9ca449e96c5dab0621d78d4'}
  });
  const [index, setIndex] = useState(5)
  const [matches, setMatches] = useState<any>([])
  const [team, setTeam] = useState<any>([])
  const [leagueTable, setLeagueTable] = useState([])
  const [header, setHeader] = useState(['Pos','Club','P', 'GD','Pts'])
  const [teamId, setTeamId] = useState()
  const tableData = []

  useEffect(()=>{
    localData()
    console.log('here here')
    console.log(teamId)
  })

  useEffect(()=>{
    // api.get('/teams/'+ props.data.teamId +'/matches',{
      api.get('/teams/'+teamId +'/matches',{
      params:{
        limit:4
      }
    }).then(res=>{
      setMatches(res.data.matches)
    })
  },[index])

  useEffect(()=>{
    // api.get('teams/'+props.data.teamId).then(res=>{
      api.get('teams/'+teamId).then(res=>{
      setTeam(res.data)
    })
  },[index])

  useEffect(()=>{
    api.get('competitions/2021/standings?standingType=TOTAL').then(res=>{
      setLeagueTable(res.data.standings[0]['table'])

    })
  },[index])

  useEffect(()=>{
    leagueTable.forEach(league  => {
      tableData.push(league.position)
    });
  },[index])

  useEffect(()=>{
    console.log('team '+team)
    console.log('matches :')
    console.log(matches)
  },[index])

  const localData = async () =>{
    try{
        let club : any = await AsyncStorage.getItem('team')
        setTeamId(parseInt(club))
    }catch(e){
        alert('error');
    }
  }

  function getWidth(column : string){
    switch(column){
      
      case 'Club':
        return 200;
      case 'ClubList':
        return 140;
      case 'logo':
        return 60
      case 'P':
      case 'GD':
      case 'Pts':
      case 'Pos':
        return 50;
    }
  }

  function getPadding(){
    return 15;
  }

  function getHeight(){
    return 50;
  }

  const viewMore = () =>{
    try{
      props.data.navigate('Details', {
        teamId: props.data.teamId
      })
    }catch(e){
      Alert('whyy? '+e)
    }
    
  }

    return (
      <View>
      
      <View style={{flex: 1, flexDirection:'row',flexWrap:'wrap'}}>
        <View style={{height: 20, width:402, borderRadius:10, alignItems:'center',backgroundColor: "#D5D5D5"}}>
            <Text style={{fontWeight:'bold'}}>Matches</Text>
          </View>
          <View style={{height: 5, width:400, borderRadius:10, alignContent:'center', paddingTop:1}}></View>
          {
            matches.map(match =>(
              <View style={{height: 150, width:102, borderRadius:10, alignItems:'center', borderWidth:1, borderColor:'grey'}}>
                <Text style={{textAlign:'center', fontWeight:'bold'}} key={match.homeTeam.name}>{match.homeTeam.name}</Text>
                {/* <Text> {match.score.fullTime.homeTeam} </Text> */}
                <Text style={{fontWeight:'bold'}}>{match.score.fullTime.homeTeam} VS {match.score.fullTime.awayTeam} </Text>
                {/* <Text> {match.score.fullTime.awayTeam} </Text> */}
                <Text style={{textAlign:'center',fontWeight:'bold'}}>{match.awayTeam.name}</Text>
                <Text style={{textAlign:'center', fontStyle:'italic'}}>{match.competition.name}</Text>
                <SvgCssUri
                  width="20%" 
                  height="20%"
                  uri={match.competition ? match.competition.area.ensignUrl : ''}
          />
              </View>
            ))
          }
          {
            matches.length != 0 ?
            <View style={{alignContent:'center', width:410}}>
            <Button title="view more"
            color="#8E1F2F"
            onPress={viewMore}
            />
            </View>
            : <Text></Text>
          }
          
          
          
          <View style={{height: 5, width:400, borderRadius:10, alignItems:'center', paddingTop:1}}></View>
          <View style={{height: 20, width:400, borderRadius:10, alignItems:'center',backgroundColor: "#D5D5D5"}}>
            <Text style={{fontWeight:'bold'}}>Club Information's</Text>
          </View>
          <View style={{height: 5, width:400, borderRadius:10, alignItems:'center', paddingTop:1}}></View>
          {/* logo */}
          <View style={{height: 200, width:150, borderRadius:10, backgroundColor:'white',alignItems:'center', paddingTop:1, borderColor:'grey'}}>
            {
              <Text style={{ fontWeight:'bold' , textAlign:'center'}}>{team.name}</Text>
            }
          <SvgCssUri
            width="50%" 
            height="50%"
            uri={team.crestUrl}
          />

            {
              team.length != 0 ?
              team.activeCompetitions.map(comp =>(
              <Text style={{textAlign:'center'}} key={comp.id}>{comp.name}</Text>
              ))
              :
              <Text>No Active competitions</Text>
            }
            
          </View>
          {/* infos */}
          <View style={{height: 200, width:252, borderRadius:10, backgroundColor:'white', 
          paddingTop:1, overflow:'hidden', alignItems:'center'}}>
            <Text style={{textAlign:'center', fontWeight:'bold'}}>SQUAD</Text>
            {
              team.length != 0 ?
              team.squad.map(each=>(
            <Text>{each.shirtNumber} {each.name}</Text>
              ))
              :
              <Text>Loading</Text>
            }
          </View>
          
            <View style={{height: 5, width:400, borderRadius:10, alignItems:'center', paddingTop:1}}></View>
            <View style={{height: 20, width:400, borderRadius:10, alignItems:'center',backgroundColor: "#D5D5D5"}}>
              <Text style={{fontWeight:'bold'}}>Legue Table</Text>
            </View>
            <View style={{height: 5, width:400, borderRadius:10, alignItems:'center', paddingTop:1}}></View>
            {
            header.map(title =>(
              <View style={{height: 40, width:getWidth(title), borderRadius:1, backgroundColor:'#8E1F2F',alignItems:'stretch', paddingTop:1, borderWidth:0}}> 
                <Text style={{textAlign:'center', fontWeight:'bold', color:'white'}}>{title}</Text>
              </View>
            ))
          }
        </View>
        <View style={{flex: 2, flexDirection:'column',flexWrap:'wrap', height:1000, alignContent:'center'}}>

          {
            leagueTable.map(league =>(
              <View style={{height: getHeight(), width:getWidth('Pos'), backgroundColor:'white',alignItems:'stretch', paddingTop:getPadding(), borderWidth:0, borderTopLeftRadius:20}}> 
                <Text style={{textAlign:'center'}}>{league.position}</Text>
              </View>
            ))
          }
          {
            leagueTable.map(league =>(
              <View style={{height: getHeight(), width:getWidth('logo'), backgroundColor:'white',alignItems:'stretch', paddingTop:10, borderWidth:0}}> 
                 <SvgCssUri
            width="80%" 
            height="80%"
            uri={league.team['crestUrl']}
          />
              </View>
            ))
          }
          {
            leagueTable.map(league =>(
              <View style={{height: getHeight(), width:getWidth('ClubList'), backgroundColor:'white',alignItems:'stretch', paddingTop:getPadding(), borderWidth:0}}> 
                <Text>{league.team['name']}</Text>
              </View>
            ))
          }
          {
            leagueTable.map(league =>(
              <View style={{height: getHeight(), width:getWidth('P'), backgroundColor:'white',alignItems:'stretch', paddingTop:getPadding(), borderWidth:0}}> 
                <Text style={{textAlign:'center'}}>{league.playedGames}</Text>
              </View>
            ))
          }
          {
            leagueTable.map(league =>(
              <View style={{height: getHeight(), width:getWidth('GD'), backgroundColor:'white',alignItems:'stretch', paddingTop:getPadding(), borderWidth:0}}> 
                <Text style={{textAlign:'center'}}>{league.goalDifference}</Text>
              </View>
            ))
          }
          {
            leagueTable.map(league =>(
              <View style={{height: getHeight(), width:getWidth('Pts'), backgroundColor:'white',alignItems:'stretch', paddingTop:getPadding(), borderWidth:0}}> 
                <Text style={{textAlign:'center'}}>{league.points}</Text>
              </View>
            ))
          }
        </View>
    </View>
      
    );
  }

  export default Teams