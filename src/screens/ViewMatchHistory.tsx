import React,{useState, useEffect} from 'react'
import {Text, ScrollView, View} from 'react-native'
import axios from 'axios'

function getColor(status: string){
    switch(status){
        case "FINISHED":
            return '#CEFFBC';
            break;
        case "POSTPONED":
            return '#FEECEB';
            break;
        default:
            return '#F0F0F0'
    }
}
export default function ViewMore(props: any){
    const api = axios.create({
        baseURL: 'https://api.football-data.org/v2/',
        headers: {'X-Auth-Token': 'a9c38bc1a9ca449e96c5dab0621d78d4'}
      });

    const [index, setIndex] = useState(false)
    const [matches, setMatches] = useState([])

    useEffect(()=>{
        api.get('/teams/'+ props.data.teamId +'/matches').then(res=>{
          setMatches(res.data.matches)
        })
      },[index])
    

    
    return(
        <ScrollView>
            <View style={{flex: 1, flexDirection:'column',flexWrap:'wrap', alignContent:"center"}}>
            {
                matches.length != 0 ? 
                matches.map(match =>(
                <View style={{height: 40, width:400, borderTopLeftRadius:20,borderBottomRightRadius:20, alignItems:'center', borderWidth:1, 
                borderColor:'grey', backgroundColor: getColor(match.status)}}>
                    <Text style={{fontWeight:'bold'}}>{match.status}</Text>
                    <Text>{match.homeTeam.name} {match.score.fullTime.homeTeam} vs {match.score.fullTime.awayTeam} {match.awayTeam.name}</Text>
                </View>
                )):
                <Text>List Not Found</Text>
            }
            </View>
        </ScrollView>

        
    )
}