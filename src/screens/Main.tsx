import React,{useState, useEffect} from 'react'
import { Button, View, Text, Image, TextInput, Picker, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import axios from 'axios'
import Config from 'react-native-config'
import AsyncStorage from '@react-native-community/async-storage'
import SwitchToggle from '@dooboo-ui/native-switch-toggle';

const styles = StyleSheet.create({
    img:{
        width: '100%',
        height: '50%',
        alignContent: 'center',
        backgroundColor: 'white',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        
    },
    defColor:{
        backgroundColor: '#f4511e',
    },

    baseText: {
        fontFamily: "Cochin",
        fontSize: 17,
      },
      titleText: {
        fontSize: 20,
        fontWeight: "bold"
      },
})


interface teamsDetail{
  name:string,
  id:number,
}
interface getTeam{
  teams:teamsDetail | any
}


function HomeScreen(props:any) {

  const api = axios.create({
    baseURL: 'https://api.football-data.org/v2/',
    headers: {'X-Auth-Token': 'a9c38bc1a9ca449e96c5dab0621d78d4'}
  });
  const [value, setValue] = useState()
  const [state, setState] = useState(false)
  const [teams, setTeams] = useState([])
  const [switchOn1, setSwitchOn1] = useState(false);
  const[selected, setSelected] = useState<number>(57)


  useEffect(()=>{
    api.get<getTeam>('teams').then(res =>{
        setTeams(res.data.teams)
    })
},[state])

const displayData = async () =>{
  try{
      let username = await AsyncStorage.getItem('name')
      let userteam = await AsyncStorage.getItem('team')
      setValue(username)
      setSelected(parseInt(userteam))
  }catch(e){
      alert('error');
  }
}

useEffect(() => {
    displayData().then()
   }, [state]);




const storeData = async () =>{
  console.log('pressed')
  console.log(props.nav)
  const username = ["name", value]
  const userteam = ["team", selected.toString()]
    try{
        await AsyncStorage.multiSet([username, userteam])
        props.nav.navigate('Teams', {
          teamId: selected,
          otherParam: 'anything you want here',
        })
    }catch(e){
        alert("error! sini eh  "+e)
    }
}

    return (
      <View>
        <Image  
            style={styles.img}
            source={require('../img/setting.png')}  
        />
        <View style={{  alignItems: 'center', backgroundColor: "#D5D5D5", borderRadius:10, paddingRight:20 }}>
            <Text>Version {Config.APP_VERSION}</Text>
        </View>
        <TextInput
                style={{ height: 40, borderColor: 'white', borderWidth: 1 }}
                onChangeText={(name)=>setValue(name)}
                value={value}
                placeholder="Your Name"
                backgroundColor="white"    
            />
            <Picker
            backgroundColor="white"
            selectedValue={selected}
            onValueChange={(itemValue, itemIndex)=>
              setSelected(itemValue)
            }
            >
                    { 
                    teams.map(each =>(
                        <Picker.Item
                        label={ each.name }
                        value={ each.id }
                        key={ each.id }
                        />
                    ))
                }
            </Picker>
        <View style={{flex:1 , flexDirection:'row', flexWrap:'wrap'}}>
          <View style={{width: 300, marginLeft:10}}>
            <Text style={{fontSize:16}}>Notification Switch</Text>
          </View>
            <SwitchToggle
            switchOn={switchOn1}
            onPress={(): void => setSwitchOn1(!switchOn1)}
            />
        </View>
        
        {/* </View> */}
        <View style={{paddingTop: 100, marginLeft:20, marginRight:20}}>
        <Button
            color="#8E1F2F"
            title="Let's GO!"
            onPress={storeData}
          />
        </View>
         
      </View>
    );
  }



  export default HomeScreen