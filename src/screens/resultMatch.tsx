import React,{useState, useEffect} from 'react'
import { Button, View, Text, Image, StyleSheet } from 'react-native';
import axios from 'axios'
import { set } from 'react-native-reanimated';
import { Table, Row, Rows } from 'react-native-table-component'

const styles = StyleSheet.create({
  box:{
      width: '100%',
      height: '50%',
      alignContent: 'center',
      backgroundColor: '#8E1F2F',
      borderBottomLeftRadius: 20,
      // borderEndColor: '#F0BC42',
      borderBottomRightRadius: 20,
      
  }
})

function ResultMatch(props) {
  const api = axios.create({
    // baseURL: 'https://api.football-data.org/v2/',
    headers: {'X-Auth-Token': 'a9c38bc1a9ca449e96c5dab0621d78d4'}
  });

  const [index, setIndex] = useState(false)
  const [matches, setMatches] = useState([])
  const [tableData, setTableData] = useState([])
  const [tableHeader, setTableHeader] = useState([])

  useEffect(()=>{
    api.get('http://api.football-data.org/v2/teams/57/matches',{
      params:{
        limit:4
      }
    }).then(res=>{
      setMatches(res.data.matches)
    })
  },[index])

    return (
      <View style={{flex: 1, flexDirection:'row', paddingLeft:5, paddingTop:10, alignSelf:'stretch'}}>
        <View style={{height: 200, borderRadius:10, alignItems:'center', paddingTop:20}}></View>
          {
            matches.map(match=>(
              <View style={{height: 150, width:100, backgroundColor: 'white', borderRadius:20, alignItems:'center', borderWidth:1}}>
                <Text>{match.homeTeam.name}</Text>
                <Text>( {match.score.fullTime.homeTeam} )</Text>
                <Text>VS</Text>
                <Text>( {match.score.fullTime.awayTeam} )</Text>
                <Text>{match.awayTeam.name}</Text>
              </View>
            ))
          }
      </View>
    );
  }

  export default ResultMatch