import React, {useState, useEffect} from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Button, View, Text, Image, TextInput, Picker, StyleSheet, Alert, ScrollView } from 'react-native';
import Homes from './src/screens/Main';
import Teams from './src/screens/Team';
import MatchHistory from './src/screens/ViewMatchHistory'
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


function HomeScreen({navigation}: any) {
 
    return (
        <View style={{ flex: 1 }}>
        <Homes nav={navigation}/>
        </View>
    )
  }
  
  function TeamsScreen({ navigation } : any) {
    // /* 2. Get the param */
    // const { teamId } = route.params;
    // const { otherParam } = route.params;
    return (
      <ScrollView>
      <View style={{ flex: 1 }}>
        
        {/* <Teams data={{...route.params, ...navigation}} /> */}
        <Teams data={navigation}/>
       
      </View>
      </ScrollView>
      
    );
  }

  function ViewMore({route, navigation} : any){
    return (
      <View style={{ flex: 1 }}>
        <MatchHistory data={{...route.params, ...navigation}} />
      </View>
    );
  }

  function getFirstPage(value: any){
    if(value == undefined){
      return HomeScreen;
    }
    return TeamsScreen;
  }

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

 function App(){

  const [index, setIndex] = useState(false)
  const [value, setValue] = useState()


const displayData = async () =>{
  try{
      let username = await AsyncStorage.getItem('name')
      setValue(username)
  }catch(e){
      alert('error');
  }
}

useEffect(() => {
    displayData().then()
   }, [index]);


    
    return(
          
            <NavigationContainer>
            <Stack.Navigator
               screenOptions={{
                headerStyle: {
                  backgroundColor: '#8E1F2F',
                },
                headerTintColor: '#fff',
            }}>
                <Stack.Screen
                name="Teams"
                component= {getFirstPage(value)}
                />
                <Stack.Screen
                name="Details"
                component={ViewMore}/>

            </Stack.Navigator>
        </NavigationContainer>
        
    )
}

export default App