import React, {useState, useEffect} from 'react'
import AsyncStorage from '@react-native-community/async-storage'
import {View, Text,TouchableOpacity} from 'react-native'


export default function storage(){
    const [name, setName] = useState('-')
    storeData = async () =>{
        try{
            await AsyncStorage.setItem('name','nor syafikah Binti Jasmin')
            alert('success');
        }catch(e){
            alert(e)
        }
    }
    displayData = async () =>{
        try{
            let user = await AsyncStorage.getItem('name')
            setName(user)
        }catch(e){
            alert('error');
        }
    }
        return(
            <View style={{paddingTop:50}}>
                <Text>storage</Text>
                <TouchableOpacity onPress={storeData}>
                    <Text>press here to save data</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={displayData}>
                    <Text>press here to display</Text>
                </TouchableOpacity>

                <Text>NAME: {name}</Text>
            </View>
        );
}

/*
*
*MULTIPLE SET IN STORAGE***
multiSet = async () => {
  const firstPair = ["@MyApp_user", "value_1"]
  const secondPair = ["@MyApp_key", "value_2"]
  try {
    await AsyncStorage.multiSet([firstPair, secondPair])
  } catch(e) {
    //save error
  }

  console.log("Done.")
}
*
*MULTIPLE GET FROM STORAGE
getMultiple = async () => {

  let values
  try {
    values = await AsyncStorage.multiGet(['@MyApp_user', '@MyApp_key'])
  } catch(e) {
    // read error
  }
  console.log(values)

  // example console.log output:
  // [ ['@MyApp_user', 'myUserValue'], ['@MyApp_key', 'myKeyValue'] ]
}
*/

