import React, { useState, useEffect} from 'react';
import { Button, View, Text, Image, Picker } from 'react-native';
import axios from 'axios';

export default function App(){

    const [anything, setAnything] = useState('wussup?')
    const [count, setCount] = useState([])
    const [employee, setEmployee] = useState([])
    const [state, setState] = useState(false)


    useEffect(()=>{
        setAnything('awesome!')
    })

    useEffect(()=>{
        setCount(['1','2','3'])
    },[state])

     useEffect(()=>{
        axios.get('http://dummy.restapiexample.com/api/v1/employees')
        .then(res =>{ 
            setEmployee(res.data.data)
        })
        .catch(err =>{
            console.log(err)
        })
    },[state])

    return(
        <View>
            <Text>Me: {anything}</Text>
            <Text>count of array would be: {count.length}</Text>
            <Text>count of employee would be: {employee.length}</Text>
            <Text>List of employee</Text>
            {
                employee.length > 0 ?
                employee.map( each =>(
                    <Text>{each.employee_name}</Text>
                )) :
                <Text> No List</Text>
            }
        </View>
    )
}