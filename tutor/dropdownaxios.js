import React, { Component, useState, useEffect} from 'react';
import { Button, View, Text, Image, Picker } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Config from 'react-native-config';
import axios from 'axios';

/*
TODO:
baseURL & headers use react-native-config
*/
const api = axios.create({
  baseURL: 'https://api.football-data.org/v2/',
  headers: {'X-Auth-Token': 'a9c38bc1a9ca449e96c5dab0621d78d4'}
});


class App extends Component{
    state = {
        teams: []
    }
    constructor(){
        
        super();
        console.log('api ->');
        api.get('/teams').then(res => {
            // console.log('****************');
            // console.log(res.data.teams);
          this.setState({ teams: res.data.teams })
        })
      }

    render(){
        return(
                <View>
                    <Text>VERSION: {Config.APP_VERSION}</Text>
                    <View>
                      <Text>Information</Text>
                        <Picker>
                        
                        {/* { this.state.teams.map(team => <Text key={team.id}>{team.id}</Text>) } */}
                        {
                              this.state.teams.map(each => (
                                <Picker.Item 
                                  label={ each.name }
                                  value={ each.name }
                                  key={ each.id }
                                />
                              ))
                        }
                        </Picker>
                    </View>
                    
                </View>
                
        )
    }
}


export default App
